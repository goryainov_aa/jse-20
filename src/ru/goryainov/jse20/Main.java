package ru.goryainov.jse20;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;


public class Main {
    public static void main(String[] args) {
        Collection<Circle> circles = new ArrayList<>();
        addFigures(circles, new Circle(1), new Circle(2));
        System.out.println("Sum area circles: " + getSumArea(circles));

        Collection<Rectangle> rectangles = new ArrayList<>();
        addFigures(rectangles, new Rectangle(1, 2), new Rectangle(2, 3));
        System.out.println("Sum area rectangles: " + getSumArea(rectangles));

        Collection<Square> squares = new ArrayList<>();
        addFigures(squares, new Square(1), new Square(2));
        System.out.println("Sum area squares: " + getSumArea(squares));

        Collection<Shape> shapes = new ArrayList<>();
        addFigures(shapes, new Rectangle(2, 3), new Square(4), new Circle(6));
        System.out.println("Sum area shapes: " + getSumArea(shapes));

    }

    @SafeVarargs
    private static <T extends Shape> void addFigures(Collection<? super T> collection, T... shapes) {
        Collections.addAll(collection, shapes);
    }

    private static <T extends Shape> double getSumArea(Collection<T> shapes) {
        double sumArea = 0;
        for (T shape : shapes) {
            sumArea += (shape).getArea();
        }
        return sumArea;
    }


}
