package ru.goryainov.jse20;

public abstract class Shape {
    public abstract double getArea();
}
