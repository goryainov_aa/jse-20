package ru.goryainov.jse20;

public class Square extends Shape{

    private double length;

    public Square(double length) {
        this.length = length;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    @Override
    public double getArea() {
        return Math.pow(length, 2);

    }
}
